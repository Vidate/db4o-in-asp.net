﻿using Db4objects.Db4o;
using OBDasp46lab2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1.Services
{
    public class DatabaseService
    {
        private IObjectContainer _dbContext;
        private string _databasePath;
        public string DatabasePath
        {
            get { return _databasePath; }
            set { _databasePath = value; }
        }

        public DatabaseService()
        {
            _databasePath = "aasdsmdmddrd.rmms0";
            list = new Dictionary<string, double>();
        }
        private void showStoredList(StoredList storedList, int deep = 1)
        {
            var res = "";
            for (int i = 0; i < deep; i++)
            {
                res += "-";
            }
            if (deep==1)
            {
                Console.WriteLine($"{storedList.Name} {storedList.MesureUnit}"); 
            }
            deep++;
            if (storedList.Elements?.Count >= 1)
            {
                foreach (var item in storedList.Elements)
                {
                    Console.WriteLine($"{res}{item.StoredList.Name} {item.Value} {storedList.MesureUnit}");
                    showStoredList(item.StoredList,deep);
                }
            }
            else
            {
                //Console.WriteLine($"{res} Have no child");
            }
        }
        public void ShowAll()
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                var result = _dbContext.Query<StoredList>().OfType<StoredList>().First();
                showStoredList(result);
                //foreach (var item in result)
                //{
                //    if (item != null)
                //    {
                //    }
                //}
                //foreach (var item in result)
                //{
                //    Console.WriteLine("StoredList: " + item.Name + " " + item.MesureUnit);
                //    if (item.Elements?.Count >= 1)
                //    {
                //        Console.WriteLine("Elements:");
                //        foreach (var element in item.Elements)
                //        {
                //            if (!(element.StoredList == null))
                //            {
                //                Console.WriteLine($"\tName: {element.StoredList.Name} Mesure unit: {element.StoredList.MesureUnit} Value: {element.Value}");
                //            }
                //            else
                //            {
                //                Console.WriteLine($"\tValue: {element.Value}");
                //            }
                //        }
                //    }
                //    else
                //    {
                //        Console.WriteLine("\tHas no elements:");
                //    }
                //}
                var cos = _dbContext.Query<StoredList>().OfType<StoredList>();
                var co2s = _dbContext.Query<Element>().OfType<Element>();
                Console.WriteLine(cos.Count());
                Console.WriteLine(co2s.Count());
            }
        }
        public bool AddStoredList(StoredList storedList)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                if (!_dbContext.Query<StoredList>().Any<StoredList>(x => x.Name.ToLower() == storedList.Name.ToLower()))
                {
                    _dbContext.Store(storedList);
                    _dbContext.Commit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        private bool checkReq(IObjectContainer _dbcontext, StoredList storedList, StoredList element)
        {
            bool _res = true;
            if (storedList.Elements?.Count >= 1)
            {
                foreach (var item in storedList.Elements)
                {
                    _res = checkReq(_dbcontext, item.StoredList, element);
                    if (_res == false)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return storedList.Name == element.Name ? false : true;
            }
            return _res;
        }
        public bool AddElementToStoredList(StoredList storedList, StoredList element, double quantity)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    var elemenFromtDb = _dbContext.Query<StoredList>().Where(x => x.Name == element.Name && x.MesureUnit == element.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    bool can = checkReq(_dbContext, storedListFromDb, elemenFromtDb);
                    bool can2 = checkReq(_dbContext, elemenFromtDb, storedListFromDb);
                    if (!storedListFromDb.Elements.Any(x => x.StoredList.Name == element.Name && x.StoredList.MesureUnit == element.MesureUnit)
                          && storedList.Name != element.Name
                          && (can == true && can2 == true)
                          && elemenFromtDb != null
                          )
                    {
                        storedListFromDb.Elements.Add(new Element() { Value = quantity, StoredList = elemenFromtDb });
                        _dbContext.Store(storedListFromDb.Elements);
                        _dbContext.Commit();
                        return true;
                    }
                    else { return false; }
                }
                catch (Exception) { return false; }
            }
        }
        public bool RemoveElementFromList(StoredList storedList, StoredList element)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    var toDelete = storedListFromDb.Elements.Where(x => x.StoredList.Name == element.Name && x.StoredList.MesureUnit == element.MesureUnit).OfType<Element>().FirstOrDefault();
                    if (toDelete == null)
                    {
                        return false;
                    }
                    storedListFromDb.Elements.Remove(toDelete);
                    _dbContext.Store(storedListFromDb.Elements);
                    _dbContext.Delete(toDelete);
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public bool ChalangeMesureUnit(StoredList storedList, string newMesureUnit)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>();
                    foreach (var item in storedListFromDb)
                    {
                        item.MesureUnit = newMesureUnit;
                        _dbContext.Store(item);
                    }
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public bool ChalangeElementValue(StoredList storedList, StoredList elementToChalange, double newValue)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    var itemToChalange = storedListFromDb.Elements.Where(x => x.StoredList.Name == elementToChalange.Name && x.StoredList.MesureUnit == elementToChalange.MesureUnit).FirstOrDefault();
                    itemToChalange.Value = newValue;
                    _dbContext.Store(itemToChalange);
                    _dbContext.Store(storedListFromDb.Elements);
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }



        public bool RemoveStoredList(StoredList storedList)
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            //_db4oConfiguration.Common.UpdateDepth = int.MaxValue;

            //_db4oConfiguration.Common.ObjectClass(typeof(Element)).CascadeOnDelete(true);
            //_db4oConfiguration.Common.ObjectClass(typeof(StoredList)).CascadeOnDelete(true);
            //_db4oConfiguration.Common.ObjectClass(new List<Element>()).CascadeOnDelete(true);
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                try
                {
                    var elementInStoredList = _dbContext.Query<StoredList>()
                        .Where(x => x.Elements.Any(y => y.StoredList.MesureUnit == storedList.MesureUnit && y.StoredList.Name == storedList.Name));
                    foreach (var item in elementInStoredList)
                    {
                        var toDelete = item.Elements.Where(x => x.StoredList.Name == storedList.Name && x.StoredList.MesureUnit == storedList.MesureUnit).OfType<Element>().FirstOrDefault();
                        if (toDelete == null)
                        {
                            return false;
                        }
                        item.Elements.Remove(toDelete);
                        _dbContext.Store(item.Elements);
                        _dbContext.Delete(toDelete);
                    }
                    var storedListFromDb = _dbContext.Query<StoredList>()
                        .Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>();
                    foreach (var item in storedListFromDb)
                    {
                        _dbContext.Delete(item);
                    }
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }
        private Dictionary<string, double> list;
        private void zlicz(StoredList storedList)
        {
            if (storedList.Elements?.Count >= 1)
            {
                foreach (var item in storedList.Elements)
                {
                    list.Add(item.StoredList.Name, item.Value);
                    zlicz(item.StoredList);
                }
            }
        }
        public void order(StoredList storedList)
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                try
                {
                    list = new Dictionary<string, double>();
                    var storedListRes = _dbContext.Query<StoredList>()
                        .Where(x => x.MesureUnit == storedList.MesureUnit
                        && x.Name == storedList.Name).OfType<StoredList>().FirstOrDefault();
                    zlicz(storedListRes);
                    foreach (var item in list)
                    {
                        Console.WriteLine($"{item.Key}  {item.Value}");
                    }
                }
                catch (Exception) { }
            }
        }
        //void EditElementInStoredList(StoredList storedList, Element element);
        //void EditItem(StoredList storedListOrginal, StoredList storedListNew);
        //void RemoveElementFromStoredList(StoredList storedList, Element element);
        //StoredList GetSotredList(StoredList storedList);
        //IEnumerable<StoredList> GetStoredLists { get; }
        //void RemoveItem(StoredList storedList);
        //void StoreItem(StoredList storedList);
    }
}
