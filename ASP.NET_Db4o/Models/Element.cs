﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_Db4o.Models
{
    public class Element
    {
        public double Value { get; set; }
        public StoredList StoredList { get; set; }
        public Element()
        {
            StoredList = new StoredList();
        }
    }
}