﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_Db4o.Models
{
    public class StoredList
    {
        public string Name { get; set; }
        public string MesureUnit { get; set; }
        public List<Element> Elements { get; set; }
        public StoredList()
        {
            Elements = new List<Element>();
        }
    }
}