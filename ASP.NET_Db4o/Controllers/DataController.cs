﻿using ASP.NET_Db4o.Models;
using ASP.NET_Db4o.Services;
using ASP.NET_Db4o.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ASP.NET_Db4o.Controllers
{
    public class DataController : Controller
    {
        private readonly IDatabaseService _databaseService;
        private readonly JsTreeService _jsTreeService;
        public DataController()
        {
            _databaseService = new DatabaseService("przykladowa");
            _jsTreeService = new JsTreeService(_databaseService);
        }
        // GET: Data
        public ActionResult ViewTree(string name, string unit)
        {
            return View("StoredListTree", new StViewModel() { name = name, unit = unit });
        }
        [HttpPost]
        public ActionResult AddStoredList()
        {
            var name = Request["Name"];
            var unit = Request["Unit"];
            double quantity = Convert.ToDouble(Request["Quantity"]);
            _databaseService.AddStoredList(new StoredList()
            {
                Name = name,
                MesureUnit = unit
            });
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult AddToStoredList(EditStoredListViewModel editStoredListViewModel)
        {
            var name = Request["Name"];
            var unit = Request["Unit"];
            double quantity = Convert.ToDouble(Request["Quantity"]);
            _databaseService.AddStoredList(new StoredList()
            {
                Name = name,
                MesureUnit = unit
            });
            _databaseService.AddElementToStoredList(new StoredList()
            {
                Name = editStoredListViewModel.ChildName,
                MesureUnit = editStoredListViewModel.ChildUnit
            }, new StoredList()
            {
                Name = name,
                MesureUnit = unit
            }, quantity);
            return View("StoredListEdit", editStoredListViewModel);
        }
        public ActionResult EditStoredList(string childName, string childUnit, string parentName, string parentUnit, string childValue)
        {
            return View("StoredListEdit", new EditStoredListViewModel()
            {
                ChildName = childName,
                ChildUnit = childUnit,
                ParentName = parentName,
                ParentUnit = parentUnit,
                ChildValue = childValue
            });
        }
        [HttpPost]
        public ActionResult ChalangeUnit(ChalangeUnitViewModel chalangeUnitViewModel)
        {
            Console.WriteLine(_databaseService.ChalangeMesureUnit(new StoredList()
            {
                Name = chalangeUnitViewModel.ChildName,
                MesureUnit = chalangeUnitViewModel.ChildUnit
            }, chalangeUnitViewModel.NewUnit));

            return View("StoredListEdit",
                new EditStoredListViewModel()
                {
                    ChildName = chalangeUnitViewModel.ChildName,
                    ChildUnit = chalangeUnitViewModel.NewUnit,
                    ParentName = chalangeUnitViewModel.ParentName,
                    ParentUnit = chalangeUnitViewModel.ParentName,
                    ChildValue = chalangeUnitViewModel.ChildValue
                });
        }
        public ActionResult DeleteFromStoredList(string name, string unit, string parentN, string parentU)
        {
            _databaseService.RemoveElementFromList(new StoredList() { Name = parentN, MesureUnit = parentU },
                new StoredList() { Name = name, MesureUnit = unit });

            return RedirectToAction("Index");
        }
        public ActionResult DeleteStoredList(string name, string unit)
        {
            _databaseService.RemoveStoredList(new StoredList() { Name = name, MesureUnit = unit });

            return RedirectToAction("Index");
        }

        public ActionResult ChalangeQuantity(EditStoredListViewModel editStoredListViewModel)
        {
            var quantity = Convert.ToDouble(Request["Quantity"]);
            _databaseService.ChalangeElementValue(new StoredList()
            {
                Name = editStoredListViewModel.ParentName,
                MesureUnit = editStoredListViewModel.ParentUnit,
            }, new StoredList()
            {
                Name = editStoredListViewModel.ChildName,
                MesureUnit = editStoredListViewModel.ChildUnit
            }, quantity);
            editStoredListViewModel.ChildValue = quantity.ToString();
            return View("StoredListEdit", editStoredListViewModel);
        }
        public JsonResult GetJsTree3Data(string name, string unit)
        {
            return Json(_jsTreeService
                .GetJsTree3Node(new StoredList() { Name = name, MesureUnit = unit }),
                JsonRequestBehavior.AllowGet);
        }

        // GET: Data
        public ActionResult Index()
        {
            Dictionary<StoredList, Dictionary<string, double>> d = new Dictionary<StoredList, Dictionary<string, double>>();
            foreach (var item in _databaseService.GetStoredListColl())
            {
                _databaseService.order(item);
                d.Add(item, _databaseService.List);
            }
            return View(new OrderViewModel() { List = d });
        }
    }
}