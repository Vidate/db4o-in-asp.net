﻿using ASP.NET_Db4o.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using jsTree3.Models;

namespace ASP.NET_Db4o.Services
{
    public class JsTreeService
    {
        private readonly IDatabaseService _databaseService;
        public JsTreeService(IDatabaseService databaseService)
        {
            this._databaseService = databaseService;
        }
        public JsTree3Node GetJsTree3Node(StoredList storedList)
        {
            var storedListRes = _databaseService.GetStoredList(storedList);
            var children = new List<JsTree3Node>();
            var result = new JsTree3Node()
            {
                text = $"{storedListRes.Name} {storedListRes.MesureUnit} 1",
                id = $"{storedListRes.Name} {storedListRes.MesureUnit} 1",
                children = GetChildrenList(storedListRes),
                state = new State(true, false, false)
            };
            return result;
        }

        public List<JsTree3Node> GetChildrenList(StoredList storedList)
        {
            var result = new List<JsTree3Node>();
            foreach (var item in storedList.Elements)
            {
                var children = new List<JsTree3Node>();
                if (item.StoredList.Elements?.Count >= 1)
                {
                    children = GetChildrenList(item.StoredList);
                }
                result.Add(new JsTree3Node()
                {
                    id = $"{item.StoredList.Name} {item.StoredList.MesureUnit} {item.Value}",
                    text = $"{item.StoredList.Name} {item.StoredList.MesureUnit} {item.Value}",
                    children = children,
                    state = new State(true, false, false)
                });
            }
            return result;
        }
    }
}