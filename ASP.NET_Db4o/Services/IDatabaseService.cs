﻿using ASP.NET_Db4o.Models;
using System.Collections.Generic;

namespace ASP.NET_Db4o.Services
{
    public interface IDatabaseService
    {
        string DatabasePath { get; set; }

        bool AddElementToStoredList(StoredList storedList, StoredList element, double quantity);
        bool AddStoredList(StoredList storedList);
        bool ChalangeElementValue(StoredList storedList, StoredList elementToChalange, double newValue);
        bool ChalangeMesureUnit(StoredList storedList, string newMesureUnit);
        StoredList GetStoredList(StoredList storedList);
        Dictionary<string, double> List { get; set; }
        ICollection<StoredList> GetStoredListColl();
        void order(StoredList storedList);
        bool RemoveElementFromList(StoredList storedList, StoredList element);
        bool RemoveStoredList(StoredList storedList);
        void ShowAll();
    }
}