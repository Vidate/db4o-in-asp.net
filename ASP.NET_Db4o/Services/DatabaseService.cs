﻿using Db4objects.Db4o;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASP.NET_Db4o.Models;

namespace ASP.NET_Db4o.Services
{
    public class DatabaseService : IDatabaseService
    {
        private IObjectContainer _dbContext;
        private string _databasePath;
        public string DatabasePath
        {
            get { return _databasePath; }
            set { _databasePath = value; }
        }

        public DatabaseService(string name)
        {
            _databasePath = name;
            List = new Dictionary<string, double>();
            //var Stół = new StoredList()
            //{
            //    Name = "Stół",
            //    MesureUnit = "szt",
            //    Elements = new List<Element>()
            //        {
            //            //(Stół, szt,((Noga,4),(Blat,1),(KompletMocowań,1)))
            //            new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Noga", MesureUnit = "szt" } },
            //            new Element () { Value= 1 ,StoredList = new StoredList() { Name = "Blat", MesureUnit = "szt" } },
            //            new Element () { Value= 1 ,StoredList = new StoredList() { Name = "KompletMocowań", MesureUnit = "kpl" } },
            //        }
            //};
            //var Noga = new StoredList()
            //{
            //    Name = "Noga",
            //    MesureUnit = "szt",
            //    Elements = new List<Element>()
            //        {
            //            //(Noga, szt,((Kantówka_30x30x600,1),(Okleina,0.0006)))
            //            new Element () { Value= 1 ,StoredList = new StoredList() { Name = "Kantówka_30x30x600", MesureUnit = "szt" } },
            //            new Element () { Value = 0.0006 ,StoredList = new StoredList() { Name = "Okleina", MesureUnit = "m2" } },
            //        }
            //};
            //var Blat = new StoredList()
            //{
            //    Name = "Blat",
            //    MesureUnit = "szt",
            //    Elements = new List<Element>()
            //        {
            //            //(Blat, szt,(Deska_600x1100x20,1),(Okleina,0.8)))
            //            new Element () { Value= 1 ,StoredList = new StoredList() { Name = "Deska_600x1100x20", MesureUnit = "szt" } },
            //            new Element () { Value= 0.8 ,StoredList = new StoredList() { Name = "Okleina", MesureUnit = "m2" } },
            //        }
            //};
            //var KompletMocowań = new StoredList()
            //{
            //    Name = "KompletMocowań",
            //    MesureUnit = "kpl",
            //    Elements = new List<Element>()
            //        {
            //            //(KompletMocowań, kpl,((Śruba,4),(Nakrętka,4),(Podkładka,4)))
            //            new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Śruba", MesureUnit = "szt" } },
            //            new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Nakrętka", MesureUnit = "szt" } },
            //            new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Podkładka", MesureUnit = "kpl" } },
            //        }
            //};
            //var Kantówka_30x30x600 = new StoredList()
            //{
            //    Name = "Kantówka_30x30x600",
            //    MesureUnit = "szt",
            //    //(Kantówka_30x30x600, szt,())
            //};
            //var Okleina = new StoredList()
            //{
            //    Name = "Okleina",
            //    MesureUnit = "m2",
            //    //(Okleina, m2,())
            //};
            //var Deska_600x1100x20 = new StoredList()
            //{
            //    Name = "Deska_600x1100x20",
            //    MesureUnit = "szt",
            //    //(Deska_600x1100x20, szt,())
            //};
            //var Śruba = new StoredList()
            //{
            //    Name = "Śruba",
            //    MesureUnit = "szt",
            //    //(Nakrętka, szt,())
            //};
            //var Nakrętka = new StoredList()
            //{
            //    Name = "Nakrętka",
            //    MesureUnit = "szt"
            //    //(Śruba, szt,())
            //};
            //var Podkładka = new StoredList()
            //{
            //    Name = "Podkładka",
            //    MesureUnit = "szt"
            //    //(Podkładka, szt,())
            //};

            //var komple = new StoredList()
            //{
            //    Name = "KompletMocowań",
            //    MesureUnit = "kpl",
            //    //Elements = new List<Element>()
            //    //{
            //    //    //(KompletMocowań, kpl,((Śruba,4),(Nakrętka,4),(Podkładka,4)))
            //    //    new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Śruba", MesureUnit = "szt" } },
            //    //    new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Nakrętka", MesureUnit = "szt" } },
            //    //    new Element () { Value= 4 ,StoredList = new StoredList() { Name = "Podkładka", MesureUnit = "kpl" } },
            //    //} 
            //};
            //this.AddStoredList(Podkładka);
            //this.AddStoredList(Nakrętka);
            //this.AddStoredList(Śruba);  
            //this.AddStoredList(komple);
            //this.AddStoredList(Kantówka_30x30x600);
            //this.AddElementToStoredList(Nakrętka, Podkładka, 786);
            //this.AddElementToStoredList(Podkładka, Śruba, 4);
        }
        public ICollection<StoredList> GetStoredListColl()
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                //var resultCollection = new List<StoredList>();
                //var collection = _dbContext.Query<StoredList>();
                //foreach (var item in collection)
                //{
                //    var results = _dbContext.Query<StoredList>()
                //        .Where(x => x.Elements.Any(y => y.StoredList.Name == item.Name)).Count();
                //    if (results < 1)
                //    {
                //        resultCollection.Add(item);
                //    }
                //}
                //return resultCollection;
                return _dbContext.Query<StoredList>().ToList();
            }
        }
        private void showStoredList(StoredList storedList, int deep = 1)
        {
            var res = "";
            for (int i = 0; i < deep; i++)
            {
                res += "-";
            }
            if (deep == 1)
            {
                Console.WriteLine($"{storedList.Name} {storedList.MesureUnit}");
            }
            deep++;
            if (storedList.Elements?.Count >= 1)
            {
                foreach (var item in storedList.Elements)
                {
                    Console.WriteLine($"{res}{item.StoredList.Name} {item.Value} {storedList.MesureUnit}");
                    showStoredList(item.StoredList, deep);
                }
            }
            else
            {
                //Console.WriteLine($"{res} Have no child");
            }
        }
        public void ShowAll()
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                var result = _dbContext.Query<StoredList>().OfType<StoredList>().First();
                showStoredList(result);
                //foreach (var item in result)
                //{
                //    if (item != null)
                //    {
                //    }
                //}
                //foreach (var item in result)
                //{
                //    Console.WriteLine("StoredList: " + item.Name + " " + item.MesureUnit);
                //    if (item.Elements?.Count >= 1)
                //    {
                //        Console.WriteLine("Elements:");
                //        foreach (var element in item.Elements)
                //        {
                //            if (!(element.StoredList == null))
                //            {
                //                Console.WriteLine($"\tName: {element.StoredList.Name} Mesure unit: {element.StoredList.MesureUnit} Value: {element.Value}");
                //            }
                //            else
                //            {
                //                Console.WriteLine($"\tValue: {element.Value}");
                //            }
                //        }
                //    }
                //    else
                //    {
                //        Console.WriteLine("\tHas no elements:");
                //    }
                //}
                var cos = _dbContext.Query<StoredList>().OfType<StoredList>();
                var co2s = _dbContext.Query<Element>().OfType<Element>();
                Console.WriteLine(cos.Count());
                Console.WriteLine(co2s.Count());
            }
        }
        public bool AddStoredList(StoredList storedList)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                if (!_dbContext.Query<StoredList>().Any<StoredList>(x => x.Name.ToLower() == storedList.Name.ToLower()))
                {
                    _dbContext.Store(storedList);
                    _dbContext.Commit();
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        private bool checkReq(IObjectContainer _dbcontext, StoredList storedList, StoredList element)
        {
            bool _res = true;
            if (storedList.Elements?.Count >= 1)
            {
                foreach (var item in storedList.Elements)
                {
                    _res = checkReq(_dbcontext, item.StoredList, element);
                    if (_res == false)
                    {
                        return false;
                    }
                }
            }
            else
            {
                return storedList.Name == element.Name ? false : true;
            }
            return _res;
        }
        public bool AddElementToStoredList(StoredList storedList, StoredList element, double quantity)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    var elemenFromtDb = _dbContext.Query<StoredList>().Where(x => x.Name == element.Name && x.MesureUnit == element.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    bool can = checkReq(_dbContext, storedListFromDb, elemenFromtDb);
                    bool can2 = checkReq(_dbContext, elemenFromtDb, storedListFromDb);
                    if (!storedListFromDb.Elements.Any(x => x.StoredList.Name == element.Name && x.StoredList.MesureUnit == element.MesureUnit)
                          && storedList.Name != element.Name
                          && (can == true && can2 == true)
                          && elemenFromtDb != null
                          )
                    {
                        storedListFromDb.Elements.Add(new Element() { Value = quantity, StoredList = elemenFromtDb });
                        _dbContext.Store(storedListFromDb.Elements);
                        _dbContext.Commit();
                        return true;
                    }
                    else { return false; }
                }
                catch (Exception) { return false; }
            }
        }
        public bool RemoveElementFromList(StoredList storedList, StoredList element)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    var toDelete = storedListFromDb.Elements.Where(x => x.StoredList.Name == element.Name && x.StoredList.MesureUnit == element.MesureUnit).OfType<Element>().FirstOrDefault();
                    if (toDelete == null)
                    {
                        return false;
                    }
                    storedListFromDb.Elements.Remove(toDelete);
                    _dbContext.Store(storedListFromDb.Elements);
                    //_dbContext.Delete(toDelete);
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }
        public bool ChalangeMesureUnit(StoredList storedList, string newMesureUnit)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>();
                    foreach (var item in storedListFromDb)
                    {
                        item.MesureUnit = newMesureUnit;
                        _dbContext.Store(item);
                    }
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }
        public bool ChalangeElementValue(StoredList storedList, StoredList elementToChalange, double newValue)
        {
            using (_dbContext = Db4oEmbedded.OpenFile(Db4oEmbedded.NewConfiguration(), _databasePath))
            {
                try
                {
                    var storedListFromDb = _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>().FirstOrDefault();
                    var itemToChalange = storedListFromDb.Elements.Where(x => x.StoredList.Name == elementToChalange.Name && x.StoredList.MesureUnit == elementToChalange.MesureUnit).FirstOrDefault();
                    itemToChalange.Value = newValue;
                    _dbContext.Store(itemToChalange);
                    _dbContext.Store(storedListFromDb.Elements);
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }

        public bool RemoveStoredList(StoredList storedList)
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            //_db4oConfiguration.Common.UpdateDepth = int.MaxValue;

            //_db4oConfiguration.Common.ObjectClass(typeof(Element)).CascadeOnDelete(true);
            //_db4oConfiguration.Common.ObjectClass(typeof(StoredList)).CascadeOnDelete(true);
            //_db4oConfiguration.Common.ObjectClass(new List<Element>()).CascadeOnDelete(true);
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                try
                {
                    var elementInStoredList = _dbContext.Query<StoredList>()
                        .Where(x => x.Elements.Any(y => y.StoredList.MesureUnit == storedList.MesureUnit && y.StoredList.Name == storedList.Name));
                    foreach (var item in elementInStoredList)
                    {
                        var toDelete = item.Elements.Where(x => x.StoredList.Name == storedList.Name && x.StoredList.MesureUnit == storedList.MesureUnit).OfType<Element>().FirstOrDefault();
                        if (toDelete == null)
                        {
                            return false;
                        }
                        item.Elements.Remove(toDelete);
                        _dbContext.Store(item.Elements);
                        _dbContext.Delete(toDelete);
                    }
                    var storedListFromDb = _dbContext.Query<StoredList>()
                        .Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).OfType<StoredList>();
                    foreach (var item in storedListFromDb)
                    {
                        _dbContext.Delete(item);
                    }
                    _dbContext.Commit();
                    return true;
                }
                catch (Exception) { return false; }
            }
        }
        public Dictionary<string, double> List { get; set; }
        private void zlicz(StoredList storedList, double ile = 1)
        {
            if (storedList.Elements?.Count >= 1)
            {
                foreach (var item in storedList.Elements)
                {
                    if (item.StoredList.Elements.Count < 1)
                    {
                        if (List.Any(x=>x.Key == item.StoredList.Name))
                        {
                            List[item.StoredList.Name] += item.Value * ile;
                        }
                        else
                        {
                        List.Add(item.StoredList.Name, item.Value * ile);
                        }
                    }
                    else
                    {
                        zlicz(item.StoredList, item.Value * ile);
                    }
                }
            }
        }
        public void order(StoredList storedList)
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                try
                {
                    List = new Dictionary<string, double>();
                    var storedListRes = _dbContext.Query<StoredList>()
                        .Where(x => x.MesureUnit == storedList.MesureUnit
                        && x.Name == storedList.Name).OfType<StoredList>().FirstOrDefault();
                    zlicz(storedListRes);
                    foreach (var item in List)
                    {
                        Console.WriteLine($"{item.Key}  {item.Value}");
                    }
                }
                catch (Exception) { }
            }
        }

        public StoredList GetStoredList(StoredList storedList)
        {
            var _db4oConfiguration = Db4oEmbedded.NewConfiguration();
            _db4oConfiguration.Common.ActivationDepth = int.MaxValue;
            using (_dbContext = Db4oEmbedded.OpenFile(_db4oConfiguration, _databasePath))
            {
                return _dbContext.Query<StoredList>().Where(x => x.Name == storedList.Name && x.MesureUnit == storedList.MesureUnit).ToList().FirstOrDefault();
            }
        }
        //void EditElementInStoredList(StoredList storedList, Element element);
        //void EditItem(StoredList storedListOrginal, StoredList storedListNew);
        //void RemoveElementFromStoredList(StoredList storedList, Element element);
        //StoredList GetSotredList(StoredList storedList);
        //IEnumerable<StoredList> GetStoredLists { get; }
        //void RemoveItem(StoredList storedList);
        //void StoreItem(StoredList storedList);
    }
}