﻿using ASP.NET_Db4o.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_Db4o.ViewModels
{
    public class OrderViewModel
    {
        public Dictionary<StoredList, Dictionary<string, double>> List { get; set; }
    }
}