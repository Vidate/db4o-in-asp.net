﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ASP.NET_Db4o.ViewModels
{
    public class EditStoredListViewModel
    {
        public string  ParentName { get; set; }
        public string  ParentUnit { get; set; }
        public string ChildValue { get; set; }


        public string ChildName { get; set; }
        public string ChildUnit { get; set; }

    }
}